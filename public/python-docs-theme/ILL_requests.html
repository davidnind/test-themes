
<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8" />
    <title>ILL requests / Interlibrary loans &#8212; Koha Manual 20.05 documentation</title>
    <link rel="stylesheet" href="_static/pydoctheme.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    
    <script id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
    <script src="_static/jquery.js"></script>
    <script src="_static/underscore.js"></script>
    <script src="_static/doctools.js"></script>
    <script src="_static/language_data.js"></script>
    
    <link rel="author" title="About these documents" href="about.html" />
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Implementation Checklist" href="implementation_checklist.html" />
    <link rel="prev" title="About Koha" href="about.html" />
    <link rel="shortcut icon" type="image/png" href="_static/py.png" />
    
    <script type="text/javascript" src="_static/copybutton.js"></script>
    
     

  </head><body>  
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             accesskey="I">index</a></li>
        <li class="right" >
          <a href="implementation_checklist.html" title="Implementation Checklist"
             accesskey="N">next</a> |</li>
        <li class="right" >
          <a href="about.html" title="About Koha"
             accesskey="P">previous</a> |</li>
    <li><img src="_static/py.png" alt=""
             style="vertical-align: middle; margin-top: -1px"/></li>
    <li><a href="https://www.python.org/">Python</a> &#187;</li>
    
    <a href="index.html">Koha Manual 20.05 documentation</a> &#187;
    

    <li class="right">
        

    <div class="inline-search" style="display: none" role="search">
        <form class="inline-search" action="search.html" method="get">
          <input placeholder="Quick search" type="text" name="q" />
          <input type="submit" value="Go" />
          <input type="hidden" name="check_keywords" value="yes" />
          <input type="hidden" name="area" value="default" />
        </form>
    </div>
    <script type="text/javascript">$('.inline-search').show(0);</script>
         |
    </li>

      </ul>
    </div>    

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <div class="section" id="ill-requests-interlibrary-loans">
<span id="ill-requests-label"></span><h1>ILL requests / Interlibrary loans<a class="headerlink" href="#ill-requests-interlibrary-loans" title="Permalink to this headline">¶</a></h1>
<p>The ILL (Interlibrary loans) requests module adds the ability to request and manage loans or copies of material from external sources. Patrons submit a request form via the OPAC for review and processing by library staff. Alternatively staff can place requests themselves from the staff client.</p>
<div class="section" id="set-up">
<span id="setup-ill-requests-label"></span><h2>Set up<a class="headerlink" href="#set-up" title="Permalink to this headline">¶</a></h2>
<p>Before using the ILL requests module you will want to make sure that you have completed all of the set up.</p>
<p>The ILL requests module can be configured for different types of requests and workflows known as backends. Currently available backends are documented on the Koha community wiki at <a class="reference external" href="https://wiki.koha-community.org/wiki/ILL_backends">https://wiki.koha-community.org/wiki/ILL_backends</a>. You will need to configure at least one backend.</p>
<p>Next, set your system preferences.</p>
<ul class="simple">
<li><p>Set the <a class="reference internal" href="systempreferences.html#illmodule"><span class="std std-ref">ILLModule</span></a> preference to ‘Enable’.</p></li>
<li><p>If you wish to include a copyright declaration in your ILL workflow you can this in the <a class="reference internal" href="systempreferences.html#illmodulecopyrightclearance"><span class="std std-ref">ILLModuleCopyrightClearance</span></a> preference.</p></li>
</ul>
<p>Library staff responsible for ILL requests need the following permission set on their account: <em>ill: Create and modify Interlibrary loan requests</em>.</p>
<p>The ILL requests module uses system defined statuses. You can add custom statuses to match your ILL workflow as ILLSTATUS <a class="reference internal" href="administration.html#authorized-values-label"><span class="std std-ref">authorized values</span></a>.</p>
</div>
<div class="section" id="create-ill-requests">
<span id="create-ill-requests-label"></span><h2>Create ILL requests<a class="headerlink" href="#create-ill-requests" title="Permalink to this headline">¶</a></h2>
<ul class="simple">
<li><p><em>Get there:</em> More &gt; ILL requests</p></li>
</ul>
<p><img alt="image1386" src="_images/ILLformpickerstaff.png" /></p>
<ul>
<li><p>Choose ‘New ILL request’ and then Freeform.</p>
<blockquote>
<div><p><strong>Note</strong></p>
<p>The images shown here are for the FreeFrom backend only.</p>
</div></blockquote>
</li>
</ul>
<p><img alt="image1387" src="_images/CreateILLstaff1.png" /></p>
<ul class="simple">
<li><p>Enter a Type: Book, Article, Journal, Other.</p></li>
<li><p>In the next two sections enter as much as information as you can including chapter/article/part if applicable.</p></li>
</ul>
<p><img alt="image1388" src="_images/createILLstaff2.png" /></p>
<ul class="simple">
<li><p>Add custom fields, for example if you wished to add a note.</p></li>
<li><p>Under the borrower options enter the library branch you would like the request to be sent to.</p></li>
</ul>
<p>Click on ‘Create’ and you will be shown a Request details summary page. Click on the ‘Confirm request’ button and you will see the following confirmation message:</p>
<p><img alt="image1389" src="_images/confirmrequeststaff.png" /></p>
<p>Click ‘Confirm request’ again to create your request.</p>
</div>
<div class="section" id="viewing-ill-requests">
<span id="viewing-ill-requests-label"></span><h2>Viewing ILL requests<a class="headerlink" href="#viewing-ill-requests" title="Permalink to this headline">¶</a></h2>
<p>From the main ILL requests screen you can see all of your ILL requests. You
can also click on the ‘View requests’ button at any time.</p>
<p><img alt="image1435" src="_images/viewillrequests1.png" /></p>
<p>There is a large amount of data available in the Requests table so it is
advisable to make use of column visibility to view only the information you need.
The first half of the table displays data related to the ILL item itself
such as title, volume, page numbers.</p>
<p><img alt="image1436" src="_images/viewillrequests2.png" /></p>
<p>The second half of the table displays data related to the request such as notes
and comments.</p>
</div>
<div class="section" id="managing-ill-requests">
<span id="managing-ill-requests-label"></span><h2>Managing ILL requests<a class="headerlink" href="#managing-ill-requests" title="Permalink to this headline">¶</a></h2>
<p>Click on the ‘Manage request’ button in the final column. Depending on the current
status of the request you may see some or all of the following options:</p>
<ul class="simple">
<li><p>Edit request</p>
<ul>
<li><p>you can edit the borrowernumber, biblionumber, branch and can add notes.</p></li>
</ul>
</li>
<li><p>Confirm request</p>
<ul>
<li><p>place the request with a document supply service using a backend such as BLDSS.</p></li>
</ul>
</li>
<li><p>Place request with partners</p>
<ul>
<li><p>place the request via email with a <a class="reference internal" href="#place-request-with-partners-label"><span class="std std-ref">partner library</span></a></p></li>
</ul>
</li>
<li><p>Delete</p>
<ul>
<li><p>fully delete the request. Details of deleted requests are not retained in Koha.</p></li>
</ul>
</li>
<li><p>Revert request</p>
<ul>
<li><p>following a status of ‘Requested’ or ‘Requested from partners’ library
staff can cancel the request from the external source. The status reverts to ‘New’</p></li>
</ul>
</li>
<li><p>Mark completed</p>
<ul>
<li><p>used when the ILL request has been fulfilled.</p></li>
</ul>
</li>
<li><p>Edit item metadata</p>
<ul>
<li><p>Depending on the backend used for the request you may be able to edit,
add or delete some or all of the request metadata.  For example, if the
metdata has originated from a requestor using the FreeForm backend this
may need to be edited, whereas metadata from an external recognised source
such as BLDSS should not.</p></li>
</ul>
</li>
<li><p>Display supplier metadata</p>
<ul>
<li><p>Displays any extra metadata that might have been provided by your ILL
supplier that has not been included in the standard request fields.</p></li>
</ul>
</li>
<li><p>Comments</p>
<ul>
<li><p>It is possible to add comments to an ILL request and these can be used
by ILL staff to keep track of work undertaken. Comments are read only and are
stored in chronological order. They display the borrower details and date of the
comment. If present, the number of comments is displayed in the List
requests view.</p></li>
</ul>
</li>
</ul>
<div class="section" id="request-statuses">
<h3>Request statuses<a class="headerlink" href="#request-statuses" title="Permalink to this headline">¶</a></h3>
<p>The full list of ILL statuses are:</p>
<ul class="simple">
<li><p>New request</p></li>
<li><p>Requested</p></li>
<li><p>Requested from partners</p></li>
<li><p>Request reverted</p></li>
<li><p>Cancellation requested – a patron has requested cancellation from the OPAC.</p></li>
<li><p>Completed</p></li>
</ul>
</div>
</div>
<div class="section" id="place-request-with-partners">
<span id="place-request-with-partners-label"></span><h2>Place request with partners<a class="headerlink" href="#place-request-with-partners" title="Permalink to this headline">¶</a></h2>
<p>If you have a network of partner libraries which permit ILL requests you can manage this through Koha. First you need to create your partner library accounts:</p>
<ul class="simple">
<li><p>You will need to set up a <a class="reference internal" href="administration.html#patron-categories-label"><span class="std std-ref">patron category</span></a> with the code ILLLIBS.</p></li>
<li><p>You will need to create a patron account with this category for each partner library.</p></li>
<li><p>Each library must have an email address as this will be the mechanism for sending the inter-library loan request.</p></li>
</ul>
<p>You can now use the ‘Place request with partners’ option when processing requests.</p>
</div>
</div>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <h3><a href="index.html">Table of Contents</a></h3>
  <ul>
<li><a class="reference internal" href="#">ILL requests / Interlibrary loans</a><ul>
<li><a class="reference internal" href="#set-up">Set up</a></li>
<li><a class="reference internal" href="#create-ill-requests">Create ILL requests</a></li>
<li><a class="reference internal" href="#viewing-ill-requests">Viewing ILL requests</a></li>
<li><a class="reference internal" href="#managing-ill-requests">Managing ILL requests</a><ul>
<li><a class="reference internal" href="#request-statuses">Request statuses</a></li>
</ul>
</li>
<li><a class="reference internal" href="#place-request-with-partners">Place request with partners</a></li>
</ul>
</li>
</ul>

  <h4>Previous topic</h4>
  <p class="topless"><a href="about.html"
                        title="previous chapter">About Koha</a></p>
  <h4>Next topic</h4>
  <p class="topless"><a href="implementation_checklist.html"
                        title="next chapter">Implementation Checklist</a></p>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="_sources/ILL_requests.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" />
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>  
    <div class="related" role="navigation" aria-label="related navigation">
      <h3>Navigation</h3>
      <ul>
        <li class="right" style="margin-right: 10px">
          <a href="genindex.html" title="General Index"
             >index</a></li>
        <li class="right" >
          <a href="implementation_checklist.html" title="Implementation Checklist"
             >next</a> |</li>
        <li class="right" >
          <a href="about.html" title="About Koha"
             >previous</a> |</li>
    <li><img src="_static/py.png" alt=""
             style="vertical-align: middle; margin-top: -1px"/></li>
    <li><a href="https://www.python.org/">Python</a> &#187;</li>
    
    <a href="index.html">Koha Manual 20.05 documentation</a> &#187;
    

    <li class="right">
        

    <div class="inline-search" style="display: none" role="search">
        <form class="inline-search" action="search.html" method="get">
          <input placeholder="Quick search" type="text" name="q" />
          <input type="submit" value="Go" />
          <input type="hidden" name="check_keywords" value="yes" />
          <input type="hidden" name="area" value="default" />
        </form>
    </div>
    <script type="text/javascript">$('.inline-search').show(0);</script>
         |
    </li>

      </ul>
    </div>  
    <div class="footer">
    &copy; <a href="copyright.html">Copyright</a> 2020, Koha Community.
    <br />

    The Python Software Foundation is a non-profit corporation.
<a href="https://www.python.org/psf/donations/">Please donate.</a>
<br />
    <br />

    Last updated on 2020-06-05 05:18:13.
    
    <br />

    Created using <a href="https://www.sphinx-doc.org/">Sphinx</a> 3.0.3.
    </div>

  </body>
</html>